#!/usr/bin/env python3

import glob
import os
import sys
import fileinput
import time
import shutil
import argparse
import sys

# TODO: Get better banner
BANNER = '__________         __         .__    ___________.__                .__                  \n\\______   \\_____ _/  |_  ____ |  |__ \\_   _____/|  | _____    _____|  |__   ___________ \n |    |  _/\\__  \\\\   __\\/ ___\\|  |  \\ |    __)  |  | \\__  \\  /  ___/  |  \\_/ __ \\_  __ \\\n |    |   \\ / __ \\|  | \\  \\___|   Y  \\|     \\   |  |__/ __ \\_\\___ \\|   Y  \\  ___/|  | \\/\n |______  /(____  /__|  \\___  >___|  /\\___  /   |____(____  /____  >___|  /\\___  >__|   \n        \\/      \\/          \\/     \\/     \\/              \\/     \\/     \\/     \\/       \n'
SUB_BANNER = "By: Vandal TheGrey"

DESCRIPTION = "A utility for quickly flashing batches of ESP8266s."


DEFAULT_PREFIX = 'painlessmesh'
DEFAULT_PORT = 5555
DEFAULT_PASS = 'password'
DEFAULT_ROLE = 'node'


print(BANNER)
print(SUB_BANNER)
print()
print(DESCRIPTION)
print()
print()

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-f', '--flash', help=".bin file to flash")
parser.add_argument('-m', '--multiple', action="store_true", help="flash multiple nodes")
args = parser.parse_args()
config = vars(args)

def flash_bin(firmware_name):
    print("Flashing node; Connect via USB...")
    port_name = get_port()
    flash(port_name, firmware_name)

    #TODO: Checking around the success of the command, and appropriate hadling.
    #      Taking into consideration keyboardEscape, etc..
    #while not flash(port_name, firmware_name):
    #    print("Flashing failed, please disconnect and reconnect the arduino")
    #    port_name = get_port()


def gen_sketch(tempFile, outFile, isRootNode = False):
    global include_root
    global root_role
    print("Generating Arduino sketch")
    skipping = False
    for line in tempFile.readlines():
        if "ROOTED END" in line:
            skipping = False
            continue
        if "ROOTED START" in line:
            skipping = True
            continue

        if not include_root and skipping:
            continue

        if "[PREFIX]" in line:
            outFile.write(line.replace('[PREFIX]', prefix))
        elif "[PASSWORD]" in line:
            outFile.write(line.replace('[PASSWORD]', password))
        elif "[PORT]" in line:
            outFile.write(line.replace('"[PORT]"', str(port)))
        elif "[ROLE]" in line:
            if isRootNode:
                outFile.write(line.replace('[ROLE]', root_role))
            else:
                outFile.write(line.replace('[ROLE]', role))
        elif 'setRoot(true)' in line:
            if include_root and isRootNode:
                outFile.write(line)
            else:
                continue
        elif 'setContainsRoot(true)' in line:
            if include_root:
                outFile.write(line)
            else:
                continue
        else:
            outFile.write(line)

prefix = ''
password = ''
port = 0
include_root = False
role = ''
root_role = ''
def main():
    global prefix
    global password
    global port
    global include_root
    global role
    global root_role
    if args.flash:
        if args.multiple:
            while True:
                flash_bin(args.flash) 
                print("Flashing done! Disconnect from USB")
                wait_port()
        else:
            flash_bin(args.flash) 
            sys.exit(0)

    prefix=input(f"Mesh prefix (SSID) [{DEFAULT_PREFIX}]: ")
    if prefix == '':
        prefix = DEFAULT_PREFIX
    password=input(f"Mesh pasword [{DEFAULT_PASS}]: ")
    if password == '':
        password = DEFAULT_PASS

    port = 0
    while port == 0:
        try:
            port_input=input(f"Port [{DEFAULT_PORT}]: ")
            if port_input == "":
                port = DEFAULT_PORT
            else:
                port = int(port_input)
        except Exception as e:
            print(e)
            pass
    include_root=None
    while include_root == None:
        iRoot=input("Include RootNode [true/yes]:")
        if (not iRoot.lower() in ['true', 't', 'yes', 'y', '']):
            include_root = False
        else:
            include_root = True

    role=input(f"Mesh node role [{DEFAULT_ROLE}]: ")
    if role == '':
        role = DEFAULT_ROLE
    root_role = role + '-root'

    os.makedirs('src/', exist_ok=True)
    os.makedirs('bin/', exist_ok=True)

    node_sketch_name = 'mesh_base.ino.' + role
    node_firmware_name = 'firmware_ESP8266_' + role + '.bin'
    tempFile = open('mesh_base.ino.template', 'r')
    newFile = open('src/' + node_sketch_name, 'w')
    gen_sketch(tempFile, newFile)
    tempFile.close()
    newFile.close()

    root_sketch_name = 'mesh_base.ino.' + root_role
    root_firmware_name = 'firmware_ESP8266_' + root_role + '.bin'
    if include_root:
        tempFile = open('mesh_base.ino.template', 'r')
        newFile = open('src/' + root_sketch_name, 'w')
        gen_sketch(tempFile, newFile, True)
        tempFile.close()
        newFile.close()

    shutil.copy('src/' + node_sketch_name, 'mesh_base.ino')
    clear_folders()
    build()
    fetch_bin(node_firmware_name)

    if include_root:
        shutil.copy('src/' + root_sketch_name, 'mesh_base.ino')
        clear_folders()
        build()
        fetch_bin(root_firmware_name)
        print("Flashing Root Node! Connect via USB...")
        port_name = get_port()
        flash_bin('bin/' + root_firmware_name)
        print("Flashing done! Disconnect from USB")
        wait_port()


    while True:
        flash_bin('bin/' + node_firmware_name)
        print("Flashing done! Disconnect from USB")
        wait_port()

def clear_folders():
    print("Cleaning Arduino Build Folders")
    arduino_build_folders = [x for x in os.listdir('/tmp') if 'arduino' in x]
    for folder in arduino_build_folders:
        shutil.rmtree('/tmp/' + folder)
            
def get_port(blocking = True):
    port_list = []
    while (len(port_list) == 0):
        port_list = glob.glob('/dev/ttyUSB*')
        if len(port_list) > 0:
            return port_list[0]
        if blocking:
            time.sleep(.5)
        else:
            return []

def wait_port():
    port_list = get_port(False)
    while len(port_list) > 0:
        time.sleep(.5)
        port_list = get_port(False)

def build():
    print("Build")
    os.system("arduino --verify --board esp8266:esp8266:generic:xtal=80,vt=flash,exception=disabled,stacksmash=disabled,ssl=all,mmu=3232,non32xfer=fast,ResetMethod=nodemcu,CrystalFreq=26,FlashFreq=40,FlashMode=dout,eesz=1M64,led=2,sdk=nonosdk_190703,ip=lm2f,dbg=Disabled,lvl=None____,wipe=none,baud=115200 mesh_base.ino --preserve-temp-files")
    

def fetch_bin(bin_name):
    print("Fetch Bin")
    arduino_build_folder = '/tmp/' + [x for x in os.listdir('/tmp') if 'arduino_build' in x][0]
    print("Fetch folder:", arduino_build_folder)
    shutil.copy(arduino_build_folder + '/mesh_base.ino.bin', 'bin/' + bin_name)

    

def flash(port_name, firmware_name):
    print("Flashing", firmware_name)
    flash_cmd = f"esptool.py --chip esp8266 --port {port_name} --baud 115200 --before default_reset --after hard_reset write_flash 0x0 {firmware_name}"
    return os.system(flash_cmd) == 0 
    # Return if command completed sucessfully

    
if __name__ == "__main__":
    main()
