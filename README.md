# BatchFlasher tool

This is a tool to help flash multiple nodes for use in a [painlessMesh](https://gitlab.com/painlessMesh/painlessMesh)-based mesh network. Normally, one would have to open the Arduino project in the IDE and compile/upload from there. In most situations, this results in a complete re-build before uploading to the next node, and that time can add up quick when flashing batches of even moderate size. 

This project allows not only for the initial flashing of a batch of mesh nodes, but facilitates the process of adding additional nodes to an existing deployment. PainlessMesh builds off of the ES8266 [OTA](https://arduino-esp8266.readthedocs.io/en/latest/ota_updates/readme.html) functionality by allowing nodes in a mesh to update themselves over the meshwork on a [per-role basis](https://gitlab.com/painlessMesh/painlessMesh/-/wikis/Over-the-air-updates-OTA). In a situation where a mesh consists of nodes of various roles, each role can be updated independently. 

Using this approach more nodes can be added, even if enhanced functionality has already been deployed; the included `.ino` sketch includes a stub with enough functionality to join a mesh and understand which role 
